<?php
/**
 * Template Name: Home
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

// Get all the posts so we can loop and see which one is marked as "Featured"
// If more than one is marked featured it will get the most recent one and display that (default)
$context['news_articles'] = Timber::get_posts( array( 'post_type' => 'news', 'posts_per_page' => 1, 'meta_key' => 'featured_article_setup', 'meta_value' => 1 ));

$context['research_articles'] = Timber::get_posts( array( 'post_type' => 'news', 'posts_per_page' => 3, 'meta_key' => 'research_article_setup', 'meta_value' => 1, 'orderby' => 'date', 'order' => 'DESC' ));

// pull in random blockquote
$rows = get_field('blockquote_or_testimonial');
$row_count = @count($rows);
$i = rand(0, $row_count - 1); // ACF starts arrays at 1 for some unknown reason
$context['quote'] = $rows[$i]['quote'];
$context['quote_author'] = $rows[$i]['quote_author'];
$context['author_title'] = $rows[$i]['author_title'];

$templates = array( 'front-page.twig' );

Timber::render( $templates, $context );