<?php

$labels = array(
	'name'               => __( 'Members', 'fhtc' ),
	'singular_name'      => __( 'Member', 'fhtc' ),
	'add_new'            => _x( 'Add Member', 'fhtc', 'fhtc' ),
	'add_new_item'       => __( 'Add Member', 'fhtc' ),
	'edit_item'          => __( 'Edit Member', 'fhtc' ),
	'new_item'           => __( 'New Member', 'fhtc' ),
	'view_item'          => __( 'View Member', 'fhtc' ),
	'search_items'       => __( 'Search Members', 'fhtc' ),
	'not_found'          => __( 'No Members found', 'fhtc' ),
	'not_found_in_trash' => __( 'No Members found in Trash', 'fhtc' ),
	'parent_item_colon'  => __( 'Parent Member:', 'fhtc' ),
	'menu_name'          => __( 'Members', 'fhtc' ),
);

// $template = array( array(
// 	// blocks here
// ));

$args = array(
	'labels'              => $labels,
	//'template'			  => $template,
	//'template_lock'	  	  => 'all',
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => array(),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => false,
	'show_in_rest'		  => true,
	'menu_position'       => 30,
	'menu_icon'           => 'dashicons-universal-access',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => false,
	'exclude_from_search' => true,
	'has_archive'         => false,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => array( 'title' ),
);
register_post_type( 'member', $args );