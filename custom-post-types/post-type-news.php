<?php

$labels = array(
	'name'               => __( 'News', 'fhtc' ),
	'singular_name'      => __( 'News', 'fhtc' ),
	'add_new'            => _x( 'Add News Article', 'fhtc', 'fhtc' ),
	'add_new_item'       => __( 'Add News Article', 'fhtc' ),
	'edit_item'          => __( 'Edit News Article', 'fhtc' ),
	'new_item'           => __( 'New News Article', 'fhtc' ),
	'view_item'          => __( 'View News Article', 'fhtc' ),
	'search_items'       => __( 'Search News Article', 'fhtc' ),
	'not_found'          => __( 'No News Articles found', 'fhtc' ),
	'not_found_in_trash' => __( 'No News Articles found in Trash', 'fhtc' ),
	'parent_item_colon'  => __( 'Parent News Article:', 'fhtc' ),
	'menu_name'          => __( 'News Articles', 'fhtc' ),
);

// $template = array( array(
// 	// blocks here
// ));

$args = array(
	'labels'              => $labels,
	//'template'			  => $template,
	//'template_lock'	  	  => 'all',
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => array(),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => false,
	'show_in_rest'		  => true,
	'menu_position'       => 30,
	'menu_icon'           => 'dashicons-lightbulb',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => true,
	'has_archive'         => true,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => array( 'title', 'thumbnail' ),
);
register_post_type( 'news', $args );