<?php
// This functions file is for all custom blocks added via ACF
// Reference: https://www.advancedcustomfields.com/resources/acf_register_block/

// Table of Contents (use cmd+f to search for the block names inside the brackets)
/**
 *  - 3 Column Grid : [three-col-grid]
 *  - 3 call to action Cards : [cta-card-row]
 *  - Full-width call to action cards : [cta-fw-card]
 *  - 2 Column Masonry Grid : [two-col-masonry]
 *  - Quote / Testimonial Block : [block-quote]
 *  - Donation Banner : [donate-banner]
 *  - Blue call to action banner : [blue-cta-banner]
 *  - Staff Card (img, name, title, etc) : [staff-card]
*/

if( function_exists('acf_register_block_type') ) :
	include 'acf-blocks-callback.php'; // pass-off to let Timber render the blocks

	/** Blocks **/
	// repeating, 3 column grid (even rows / columns)
	$three_col_grid = array(
		'name' => 'three-col-grid',
		'title' => __( 'Three Column Grid', 'fhtc' ),
		'description' => __( 'Custom 3 column grid (infinite rows).', 'fhtc' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'kwcrf-blocks',
		'align' => 'wide',
		'icon' => 'grid-view',
		'mode' => 'auto',
		'supports' => array( 'mode' => true ),
		'keywords' => array( 'three', 'column', 'grid', '3' )
	);
	acf_register_block_type( $three_col_grid );

	// single row of call to action cards (max 3)
	$cta_card_row = array(
		'name' => 'cta-card-row',
		'title' => __( 'Card Row' ),
		'description' => __( 'Creates a single row of call-to-action cards. 3 per row.' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'kwcrf-blocks',
		'align' => 'wide',
		'icon' => 'editor-insertmore',
		'mode' => 'auto',
		'supports' => array( 'mode' => true ),
		'keywords' => array( 'call', 'action', 'card', 'row' )
	);
	acf_register_block_type( $cta_card_row );

	// single card, full width
	$cta_fw_card = array(
		'name' => 'cta-fw-card',
		'title' => __( 'Full Width Card' ),
		'description' => __( 'Creates a single call-to-action card that is full width; Text on the left, image on the right.' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'kwcrf-blocks',
		'align' => 'wide',
		'icon' => 'excerpt-view',
		'mode' => 'auto',
		'supports' => array( 'mode' => true ),
		'keywords' => array( 'call', 'action', 'card', 'column' )
	);
	acf_register_block_type( $cta_fw_card );

	// two column grid with uneven columns (no rows)
	$two_col_masonry = array(
		'name' => 'two-col-masonry',
		'title' => __( 'Two Column Masonry Grid' ),
		'description' => __( 'Create a grid with 2 columns, each column has its own, independent rows.' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'kwcrf-blocks',
		'align' => 'wide',
		'icon' => 'schedule',
		'mode' => 'auto',
		'supports' => array( 'mode' => true ),
		'keywords' => array( 'two', 'column', 'masonry', 'grid', '2' )
	);
	acf_register_block_type( $two_col_masonry );

	// custom block quote
	$block_quote = array(
		'name' => 'block-quote',
		'title' => __( 'Quote / Testimonial' ),
		'description' => __( 'Creates a center, italic quote section in the page. Allows for quote, author, and author title.' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'kwcrf-blocks',
		'align' => 'wide',
		'icon' => 'editor-quote',
		'mode' => 'auto',
		'supports' => array( 'mode' => true ),
		'keywords' => array( 'quote', 'byline', 'testimonial', 'blockquote' )
	);
	acf_register_block_type( $block_quote );

	// Donate Banner
	$donate_banner = array(
		'name' => 'donate-banner',
		'title' => __( 'Donation Banner', 'fhtc' ),
		'description' => __( 'Maroon donation banner with tagline and button.', 'fhtc' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'kwcrf-blocks',
		'align' => 'wide',
		'icon' => 'editor-expand',
		'mode' => 'auto',
		'supports' => array( 'mode' => true ),
		'keywords' => array( 'donate', 'donation', 'banner', 'bar' )
	);
	acf_register_block_type( $donate_banner );

	$blue_cta_banner = array(
		'name' => 'blue-cta-banner',
		'title' => __( 'Blue CTA Banner' ),
		'description' => __( 'Creates a textured, blue call to action banner.' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'kwcrf-blocks',
		'align' => 'wide',
		'icon' => 'editor-contract',
		'mode' => 'auto',
		'supports' => array( 'mode' => true ),
		'keywords' => array( 'blue', 'banner', 'call', 'action' )
	);
	acf_register_block_type( $blue_cta_banner );

	$staff_card = array(
		'name' => 'staff-card',
		'title' => __( 'Staff Card' ),
		'description' => __( 'Creates a card for the Staff member with an image, name, role/title, and a short blurb about the person.' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'kwcrf-blocks',
		'align' => 'wide',
		'icon' => 'universal-access',
		'mode' => 'auto',
		'supports' => array( 'mode' => true ),
		'keywords' => array( 'staff', 'card', 'member', 'board', 'committee' )
	);
	acf_register_block_type( $staff_card );

	$recognition_block = array(
		'name' => 'recognition-block',
		'title' => __( 'Recognition Block' ),
		'description' => __( 'Creates a recognition block with image right and representative contact information on the left.' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'kwcrf-blocks',
		'align' => 'wide',
		'icon' => 'star-filled',
		'mode' => 'auto',
		'supports' => array( 'mode' => true ),
		'keywords' => array( 'staff', 'card', 'member', 'board', 'committee' )
	);
	acf_register_block_type( $recognition_block );

endif;