<?php
/**
 * Template Name: Staff Member
 * 
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

the_post();

$templates = array( 'staff-member.twig' );

Timber::render( $templates, $context );