<?php
/**
 * Template Name: News Archive
 * 
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$context['news_articles'] = Timber::get_posts( array( 'post_type' => 'news', 'posts_per_page' => -1, 'orderby' => 'date', 'order' => 'DESC' ));

$templates = array( 'archive-news.twig' );

Timber::render( $templates, $context );