<?php
/**
 * Default Template.
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$context['members'] = Timber::get_posts(array( 'post_type' => 'member', 'posts_per_page' => -1, 'orderby' => 'date', 'order' => 'ASC' )); // first one created is first in the list

$templates = array( 'page.twig' );

Timber::render( $templates, $context );