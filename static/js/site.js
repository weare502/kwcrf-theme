(function($) {
    $(document).ready(function() {

		// Tiny MCE adds lots of empty p tags to the ACF Wysiwyg field
		// This does not affect tags that have  <p>&nbsp;</p>  in them (or any other content)
		$('p:empty').remove();

		// mobile menu (fade & slide right to left - controlled with css)
		$(function fadeDownMenu() {
			$('#menu-toggle').click(function() {
				$('#primary-menu').toggleClass('menu-open');
			});
		});

		$(function videoToggle() {
			// video controls
			var play_video = $('#play-button');
			var video_player = $('#video-modal').get(0);
			var close_video = $('#close-video');

			play_video.click(function() {
				$('.video-modal-container').fadeIn(500);
				//$('#background-video').fadeOut(200);
				$('.darken').show();

				if(video_player.paused == true) {
					video_player.play();
					video_player.controls = true; // enable controls once video is playing
				} else {
					video_player.pause();
				}
			});

			close_video.click(function() {
				video_player.pause();
				$('.video-modal-container').fadeOut(500);
				$('.darken').hide();
			});
		});
	});
})(jQuery)