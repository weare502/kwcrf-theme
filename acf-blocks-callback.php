<?php
/**
*  This is the callback that displays the blocks.
*  !IMPORTANT! - get_fields() is ambiguous - all custom fields must have a unique name
*
* @param   array $block - The block settings and attributes.
* @param   string $content - The block content (empty string).
* @param   bool $is_preview - True during AJAX preview.
*/

function acf_custom_blocks_callback( $block, $content = '', $is_preview = false ) {
	$context = Timber::get_context();

	$context['block'] = $block;
	$context['fields'] = get_fields();
	$context['is_preview'] = $is_preview;

	//**** pull a random quote if more than one is added
	$rows = get_field('blockquote_or_testimonial');
	$row_count = @count($rows);
	$i = rand(0, $row_count - 1); // ACF starts arrays at 1 for some unknown reason
	$context['quote'] = $rows[$i]['quote'];
	$context['quote_author'] = $rows[$i]['quote_author'];
	$context['author_title'] = $rows[$i]['author_title'];
	//****

	// templates/blocks/acf/BLOCK.twig
	$template = 'templates/blocks/' . $block['name'] . '.twig';
	Timber::render( $template, $context );
}